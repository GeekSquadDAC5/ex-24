﻿using System;

namespace ex_24
{
    class Program
    {
        static void Main(string[] args)
        {
// EX#24
            // a) Create a method of type void that prints out a Console.WriteLine Message.
            // The message needs to say: “My first method”; Show this that message 5 times on your screen
            for(var i=0;i<5;i++)
            {
                Console.WriteLine(FirstMethod());
            }

            // b) Create a method that takes in a single parameter of type string
            // Have the method print out the message “Hello {name}”
            // Print this out 5 times with 5 different names.
            var names = new String[5]{"Sing", "Jazz", "Cho", "Vincent", "Jeff"};
            for(var i=0;i<5;i++)
            {
                PrintMyInfo(names[i]);
            }

            // c) Do the same as task b - but instead of type void, return a string. Print that string out to the screen
            for(var i=0;i<5;i++)
            {
                Console.WriteLine(StrMyInfo(names[i]));
            }

            // d) Multiply 3 numbers Create a method that takes 3 parameters - all Numbers
            // The method is to return a number that multiplies those 3 numbers and then you print
            // that to the screen - Set up 5 of those.
            Console.WriteLine($"2 x 3 x 34 = {IntMultiple(2, 3, 34)}");
            Console.WriteLine($"2 x 4 x 34 = {IntMultiple(2, 4, 34)}");
            Console.WriteLine($"2 x 5 x 34 = {IntMultiple(2, 5, 34)}");
            Console.WriteLine($"2 x 6 x 34 = {IntMultiple(2, 6, 34)}");
            Console.WriteLine($"2 x 7 x 34 = {IntMultiple(2, 7, 34)}");

            // e) Create a method that takes in a number. Inside the method create a simple for loop that
            // prints out the numbers 1 to the number of the paramter.
            PrintNumbers(30);
        }


        // a)
        static string FirstMethod()
        {
            return "My first method\n";
        }

        // b)
        static void PrintMyInfo(string name)
        {
            Console.WriteLine($"Hello  {name}");
        }

        // c)
        static string StrMyInfo(string name)
        {
            return "Hello " + name;
        }

        // d)
        static int IntMultiple(int num1, int num2, int num3)
        {
            return num1*num2*num3;
        }

        // e)
        static void PrintNumbers(int lastNum)
        {
            for(var i=1;i<=lastNum;i++)
            {
                Console.WriteLine(i);
            }
        }
    }
}
